const Product = require("../../models/Product");
const User = require("../../models/User");
const getPagination = require("../../util/Pagination");

// RENDER PRODUCTS IN PRODUCTS PAGE ...

exports.getProducts = async (args, request) => {
  try {
    const pagination = await getPagination(request.body.currentPage, false);
    const products = await Product.find()
      .skip(pagination.startPage)
      .limit(pagination.ITEM_PER_PAGE)
      .select("_id title price imageUrl description");

    const response = {
      products: products,
      itemsCountPerPage: pagination.ITEM_PER_PAGE,
      totalItemsCount: pagination.totalProducts
    };
    return response;
  } catch (error) {
    return error;
  }
};

// GET PRODUCT DETAILS METHOD ...
exports.getProduct = async (args, request) => {
  const productId = request.body.productId;
  try {
    const product = await Product.findById(productId).select(
      "title price imageUrl description"
    );
    return product;
  } catch (error) {
    return error;
  }
};

// RENDER CART PAGE ...
exports.getCartProducts = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }

  try {
    const user = await User.findById(request.user._id)
      .populate("cart.items.productId")
      .select("cart");
    const products = [...user.cart.items];

    return {
      products: products
    };
  } catch (error) {
    return error;
  }
};

// ADD PRODUCT INTO CART POST METHOD ...
exports.postAddProductToCart = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }

  const productId = request.body.productId;
  try {
    const user = await User.findById(request.user._id);

    const productCartIndex = user.cart.items.findIndex(cp => {
      return cp.productId.toString() === productId.toString();
    });

    const updatedCartItems = [...user.cart.items];

    let newQuantity = 1;

    if (productCartIndex !== -1) {
      newQuantity = updatedCartItems[productCartIndex].quantity + 1;
      updatedCartItems[productCartIndex].quantity = newQuantity;
    } else {
      updatedCartItems.push({
        productId,
        quantity: newQuantity
      });
    }

    const updatedCart = { items: updatedCartItems };

    user.cart = updatedCart;

    await user.save();

    return {
      success: true,
      message: "Product added to cart successfully!"
    };
  } catch (error) {
    return error;
  }
};

// DELETE PRODUCT FROM CART ...
exports.postCartDeleteProduct = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }

  const productId = request.body.productId;

  try {
    const user = await User.findById(request.user._id).select("cart");

    if (user) {
      const updatedCartItems = user.cart.items.filter(item => {
        return item.productId.toString() !== productId.toString();
      });
      user.cart.items = updatedCartItems;
      await user.save();

      return {
        success: true,
        message: "Product delete from cart successfully!"
      };
    }
  } catch (error) {
    return error;
  }
};

// INCREMENTING QUANTITY OF PRODUCT  IN CART...
exports.incrementQuantityOfProduct = async (args, request) => {
  const productId = request.body.productId;
  const quantity = request.body.quantity;

  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const user = await User.findById(request.user._id);

    const updatedCartItems = [...user.cart.items];

    const productCartIndex = user.cart.items.findIndex(cp => {
      return cp.productId.toString() === productId.toString();
    });

    if (productCartIndex === -1) {
      const error = new Error("Product Not Found !");
      error.status = 404;
      throw error;
    } else {
      updatedCartItems[productCartIndex].quantity = quantity;

      const updatedCart = { items: updatedCartItems };
      user.cart = updatedCart;

      await user.save();
      return {
        success: true,
        message: "Quantity updated successfully!"
      };
    }
  } catch (error) {
    return error;
  }
};

// DECREMENTING QUANTITY OF PRODUCT  IN CART...
exports.decrementQuantityOfProduct = async (args, request) => {
  const productId = request.body.productId;
  const quantity = request.body.quantity;
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const user = await User.findById(request.user._id);

    const updatedCartItems = [...user.cart.items];

    const productCartIndex = user.cart.items.findIndex(cp => {
      return cp.productId.toString() === productId.toString();
    });

    if (productCartIndex === -1) {
      const error = new Error("Product Not Found !");
      error.status = 404;
      throw error;
    } else {
      updatedCartItems[productCartIndex].quantity = quantity;

      const updatedCart = { items: updatedCartItems };
      user.cart = updatedCart;

      await user.save();
      return {
        success: true,
        message: "Quantity updated successfully!"
      };
    }
  } catch (error) {
    return error;
  }
};

exports.getCheckoutDetails = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }

  try {
    const user = await User.findById(request.user._id)
      .populate("cart.items.productId")
      .select("cart");

    const products = user.cart.items;
    let totalPrice = null;
    products.forEach(product => {
      totalPrice += product.productId.price * product.quantity;
    });
    return {
      products,
      totalPrice
    };
  } catch (error) {
    return error;
  }
};

exports.clearCart = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  try {
    const user = await User.findById(request.user._id).select("cart");

    user.cart = [];

    await user.save();

    return {
      message: "Cart Cleared successfully!",
      success: true
    };
  } catch (error) {
    return error;
  }
};
