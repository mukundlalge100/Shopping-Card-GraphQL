const Order = require("../../models/Order");
const User = require("../../models/User");
const path = require("path");
const fs = require("fs");
const pdfKit = require("pdfkit");
const file = require("../../util/File");
const stripe = require("stripe")(process.env.STRIPE_KEY);
const AWS = require("aws-sdk");

const s3 = new AWS.S3({
  region: "us-east-2",
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
});

// POST ORDER FOR CART PRODUCTS FOR AUTHENTICATED USERS AND CLEAR CART PRODUCTS AFTER PLACING ORDER...
exports.postOrder = async (args, request) => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const user = await User.findById(request.user._id).select(
      "_id mobileNumber userName email cart"
    );
    const stripeToken = request.body.token.id;

    const userNew = {
      _id: user._id,
      userName: user.userName,
      mobileNumber: user.mobileNumber,
      email: user.email
    };

    let userTemp = await user.populate("cart.items.productId").execPopulate();

    const productsRaw = userTemp.cart.items;

    const products = productsRaw.map(product => {
      return {
        quantity: product.quantity,
        product: { ...product.productId._doc }
      };
    });

    let totalPrice = 0;
    products.forEach(product => {
      totalPrice = product.quantity * product.product.price + totalPrice;
    });

    const order = new Order({
      user: userNew,
      products,
      totalPrice
    });
    const result = await order.save();

    // STRIPE PAYMENT ...
    (async () => {
      const charge = await stripe.charges.create({
        amount: totalPrice * 100,
        currency: "INR",
        description: "Demo Order",
        source: stripeToken,
        metadata: { orderId: result._id.toString() }
      });
    })();

    //CLEARING ALL CART ITEMS ...
    user.cart.items = [];
    await user.save();
    return {
      success: true,
      message: "Order Added successfully"
    };
  } catch (error) {
    return error;
  }
};

// RENDERING ORDER PAGE ...
exports.getOrders = async (args, request) => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const orders = await Order.find().select("products totalPrice");
    return { orders };
  } catch (error) {
    return error;
  }
};

// DOWNLOADING PDF FILE FOR INVOICE ...
exports.getInvoice = async (request, response, next) => {
  const orderId = request.body.orderId;
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const order = await Order.findById(orderId);
    if (!order) {
      return next(new Error("No order found!!"));
    }
    if (order.user._id.toString() !== request.user._id.toString()) {
      return next(new Error("Unautherized user!"));
    }
    const invoiceName = "invoice-" + orderId + ".pdf";
    const invoicePath = path.join("data", "Invoices", invoiceName);
    const pdfDocument = new pdfKit();

    response.setHeader("Content-Type", "application/pdf");
    response.setHeader(
      "Content-Disposition",
      `inline; filename = ${invoiceName}`
    );
    pdfDocument.pipe(fs.createWriteStream(invoicePath));
    pdfDocument.pipe(response);

    pdfDocument
      .fontSize(26)
      .fillColor("#cc3366")
      .text("--------------------INVOICE--------------------");
    pdfDocument
      .fontSize(20)
      .fillColor("#cc3366")
      .text("\n\n #Product Details => ");

    order.products.forEach(product => {
      pdfDocument
        .fontSize(18)
        .text(
          `\n\nProduct Name => ${product.product.title} \n\nQuantity => ${
            product.quantity
          } \n\nActual Product Price => Rs. ${
            product.product.price
          }\n\nTotal Product Price => Rs. ${product.product.price *
            product.quantity}`
        )
        .fill("#cc3366");
    });

    pdfDocument
      .fontSize(26)
      .fillColor("#cc3366")
      .text(`\n\n\n-----Total Grand Price => $${order.totalPrice} -----`);

    pdfDocument.image("./shoppingKingLogo.png", 270, 10, {
      height: 50,
      width: 50,
      align: "center",
      valign: "center"
    });

    pdfDocument.end();
  } catch (error) {
    console.log(error);
  }
};

// DELETE ORDER FROM UI AND DATABASE ...
exports.deleteOrder = async (args, request) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const orderId = request.body.orderId;
  const invoiceName = "invoice-" + orderId + ".pdf";
  const invoicePath = path.join("data", "Invoices", invoiceName);

  try {
    const order = await Order.findByIdAndDelete(orderId);
    if (!order) {
      const error = new Error("Order not found !");
      error.status = 404;
      throw error;
    }
    file.deleteFile(invoicePath);
    return {
      success: true,
      message: "ORDER DELETED SUCCESSFULLY!"
    };
  } catch (error) {
    return error;
  }
};
