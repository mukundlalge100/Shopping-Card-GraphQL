const Product = require("../../models/Product");
const file = require("../../util/File");
const getPagination = require("../../util/Pagination");
const AWS = require("aws-sdk");

const s3 = new AWS.S3({
  region: "us-east-2",
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
});

// POST ADDPRODUCT REQUEST...
exports.postAddProduct = async (request, response, next) => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }

    const title = request.body.title;
    const imageUrl = request.file.location;
    const price = request.body.price;
    const description = request.body.description;
    console.log(imageUrl);
    const newProduct = new Product({
      title,
      imageUrl,
      price,
      description,
      userId: request.user._id
    });
    const product = await newProduct.save();
    response.json(product);
  } catch (error) {
    console.log(error);
  }
};

// GET REQUEST FOR RENDERING ADMIN PRODUCTS...
exports.getProductList = async (args, request) => {
  try {
    if (!request.isAuth) {
      const error = new Error("Unauthorized User!");
      error.status = 401;
      throw error;
    }
    const pagination = await getPagination(
      request.body.currentPage,
      request.user._id
    );

    const products = await Product.find({ userId: request.user._id })
      .skip(pagination.startPage)
      .limit(pagination.ITEM_PER_PAGE)
      .select("_id title price imageUrl description");

    const response = {
      products: products,
      itemsCountPerPage: pagination.ITEM_PER_PAGE,
      totalItemsCount: pagination.totalProducts
    };
    return response;
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR EDITING PRODUCT...
exports.postEditProduct = async (request, response, next) => {
  if (!request.isAuth) {
    const error = new Error("Unauthorized User!");
    error.status = 401;
    throw error;
  }
  const title = request.body.title;
  const price = request.body.price;
  const description = request.body.description;
  const productId = request.body.productId;
  const image = request.file;

  try {
    const product = await Product.findOne({ _id: productId }).select(
      "userId imageUrl title price description"
    );

    if (!product) {
      return next(new Error("Product not found!!"));
    }

    if (product) {
      if (product.userId.toString() !== request.user._id.toString()) {
        const error = new Error("Unauthorized User!");
        error.status = 401;
        throw error;
      }
    }

    let imageUrl = product.imageUrl;
    // IF THERE IS IMAGE IN REQUEST DELETE PREVIOUS IMAGE AND CREATE NEW UPDATED IMAGE ...
    if (image) {
      const imageUrlArray = imageUrl.split(".com/");
      const fileName = imageUrlArray[1];
      const params = {
        Bucket: "shoppingcard",
        Key: fileName
      };
      s3.deleteObject(params, (err, data) => {
        if (err) {
          console.log(err);
        } else {
          console.log(data);
        }
      });
      imageUrl = image.location;
    }
    const updatedProduct = await Product.findByIdAndUpdate(
      { _id: productId },
      { title, imageUrl, price, description }
    );
    response.json(updatedProduct);
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// POST REQUEST FOR DELETING PRODUCT ...
exports.deleteProduct = async (args, request) => {
  const productId = request.body.productId;
  try {
    const product = await Product.findOne({ _id: productId }).select(
      "userId imageUrl"
    );

    if (product) {
      if (product.userId.toString() !== request.user._id.toString()) {
        const error = new Error("Unauthorized User!");
        error.status = 401;
        throw error;
      }
    }

    // DELETE IMAGE FROM SERVER AFTER DELETING PRODUCT ...
    const imageUrlArray = product.imageUrl.split(".com/");
    const fileName = imageUrlArray[1];
    const params = {
      Bucket: "shoppingcard",
      Key: fileName
    };
    s3.deleteObject(params, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(data);
      }
    });

    const result = await Product.findByIdAndDelete({
      _id: productId
    });
    if (result) {
      return {
        success: true,
        message: "Product deleted successfully!!"
      };
    }
  } catch (error) {
    return error;
  }
};
