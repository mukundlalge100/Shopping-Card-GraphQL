exports.get404Error = (request, response, next) => {
  response.status(404).render("404", {
    pageTitle: "Page not found!",
    path: "/404"
  });
};
