exports.get505Error = (request, response, next) => {
  response.status(505).render("505", {
    pageTitle: "Something went wrong!",
    path: "/505"
  });
};
