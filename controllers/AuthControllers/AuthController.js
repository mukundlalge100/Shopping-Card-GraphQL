const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const validateUserSignUpInput = require("../../validations/SignUpValidations");
const validateUserLogInInput = require("../../validations/LogInValidations");
const validateNewPasswordInput = require("../../validations/NewPasswordValidations");
const jwt = require("jsonwebtoken");

// SENDGRID NODEMAILER CONFIG...
const nodeMailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");

try {
  var transportor = nodeMailer.createTransport(
    sendGridTransport({
      auth: {
        api_key: process.env.SENDGRID_API_KEY,
      },
    })
  );
} catch (error) {
  console.log(error);
}

// POST REQUEST FOR USER LOGIN ...
exports.postLogIn = async (args, request) => {
  try {
    const password = args.password;
    const email = args.email;
    // CHECKING VALIDATION ALL USER INPUT FIELDS...
    const { errors, isValid } = validateUserLogInInput(args);
    if (!isValid) {
      const error = new Error("Invalid Inputs!!");
      error.status = 422;
      error.errors = errors;
      throw error;
    }
    // CHECKING IF USER EXIST OR NOT IN DB...
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Email is not exists,Please enter valid email!");
      error.errors = {
        emailIsNotExist: "Email is not exists,Please enter valid email!",
      };
      error.status = 420;
      throw error;
    }
    // CHECK IF PASSWORD IS VALID OR NOT ...
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      const error = new Error(
        "Password credentials are not valid,please enter valid credentials!"
      );
      error.errors = {
        passwordIsNotValid:
          "Password credentials are not valid,please enter valid credentials!",
      };
      error.status = 420;
      throw error;
    }
    const updatedUser = {
      _id: user._id,
      userName: user.userName,
      email: user.email,
    };
    // IF VALIDATIONS SUCCESSFUL ,ADD USER IN SESSIONS AND LOGGED IN USER...
    request.session.user = updatedUser;
    request.session.isLoggedIn = true;
    request.session.save((error) => {
      if (error) {
        const err = new Error(error);
        err.status = 505;
        throw err;
      }
    });
    // JWT TOKEN AFTER SIGN IN ...
    const token = jwt.sign(updatedUser, process.env.SECRET_OR_KEY, {
      expiresIn: 86400,
    });
    return { token: `Bearer ${token}` };
  } catch (error) {
    return error;
  }
};

exports.validateEmail = async (args, request) => {
  const email = args.email;

  try {
    const userExist = await User.findOne({ email: email }).select("email -_id");

    if (userExist) {
      return {
        success: false,
        message: "Email is already taken,please try different one!",
      };
    } else {
      return {
        success: true,
        message: "Email is not taken!",
      };
    }
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR REGISTERING USER IN OUR APP...
exports.postSignUp = async (args, request) => {
  const userName = args.userName;
  const mobileNumber = args.mobileNumber;
  const email = args.email;
  const password = args.password;

  // CHECKING VALIDATION ALL USER INPUT FIELDS...
  const { errors, isValid } = validateUserSignUpInput(args);
  if (!isValid) {
    const error = new Error("Invalid Inputs!!");
    error.status = 422;
    error.errors = errors;
    throw error;
  }

  try {
    // CHECK IF USER IS ALREADY EXIST OR NOT ...
    const userExist = await User.findOne({ email: email }).select("email -_id");

    if (userExist) {
      const error = new Error(
        "Email is already exists,Please enter valid email!"
      );
      error.errors = {
        emailIsAlreadyExist:
          "Email is  already exists,Please enter valid email!",
      };
      error.status = 420;
      throw error;
    }

    // ENCRYPT PASSWORD BEFORE STORING IT INTO DATABASE...
    const bcryptPassword = await bcrypt.hash(password, 16);

    const user = new User({
      userName,
      mobileNumber,
      email,
      password: bcryptPassword,
      cart: { items: [] },
    });
    // AFTER VALIDATIONS SUCCESSFUL,SAVE USER IN DATABASE ...
    await user.save();

    // AFTER SUCCESSFUL SIGNUP, SEND EMAIL TO USER FOR SUCCESSFUL SIGNED UP USING SENDGRID AND NODEMAILER ...
    transportor.sendMail({
      to: email,
      from: "mukundlalge49@gmail.com",
      subject: "Congratulations,sign up succeeded!",
      html:
        "<h1>You have successfully signed up!!</h1> <br><h2>Welcome to ShopiFier !</h2>",
    });

    return { success: true, message: "Signup successfully!" };
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR RESSETING USER PASSWORD ...
exports.postPasswordReset = async (args, request) => {
  // GENERATING TOKEN USING CRYPTO MODULE ...
  const token = crypto.randomBytes(32).toString("hex");

  const email = args.email;

  try {
    // IF USER IS ALREADY EXISTS,THEN REDIRECT USER TO RESET PASSWORD PAGE WITH ERROR MESSAGES ...
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Email is not exists,Please enter valid email!");
      error.errors = {
        emailIsNotExist: "Email is not exists,Please enter valid email!",
      };
      error.status = 420;
      throw error;
    }
    user.resetToken = token;
    user.resetTokenExpiration = Date.now() + 3600000;
    // SAVE TOKEN AND TOKEN EXPIRATION TIME AFTER USER REQUEST FOR RESETING PASSWORD...
    await user.save();

    // AFTER SAVING TOKEN SEND USER EMAIL LINK FOR CHANGING PASSWORD...
    transportor.sendMail({
      to: email,
      from: "mukundlalge49@gmail.com",
      subject: "Reset your password",
      html: `<h2>Request for resetting your password</h2>
        <p>Click here <a href = https://ancient-coast-41651.herokuapp.com/reset-password/${token}>to set new password</a>.</p>`,
    });
    // REDIRECT USER AFTER SAVING AND SENDING EMAIL TO USER...
    return {
      success: true,
      message: "Email is send to your email address for resetting password!",
    };
  } catch (error) {
    return error;
  }
};

// POST REQUEST FOR CHANGING USER PASSWORD...
exports.postNewPassword = async (args, request) => {
  const password = args.password;

  const token = args.token;
  let resetUser;

  // CHECKING VALIDATION ALL USER INPUT FIELDS...
  const { errors, isValid } = validateNewPasswordInput(args);
  if (!isValid) {
    const error = new Error("Invalid Inputs!!");
    error.status = 422;
    error.errors = errors;
    throw error;
  }

  try {
    // IF USER HAS VALID TOKEN OR NOT ...
    const user = await User.findOne({
      resetToken: token,
      resetTokenExpiration: {
        $gt: Date.now(),
      },
    }).select("_id");

    if (!user) {
      // IF TOKEN IS INVALID OR EXPIRED REDIRECT USER TO RESET PASSWORD PAGE...
      const error = new Error("Invalid token or token has expired!");
      error.errors = {
        invalidToken: "Invalid token or token has expired!",
      };
      error.status = 401;
      throw error;
    }
    // ENCRYPT PASSWORD BEFORE STORING IT INTO DATABASE...
    const bcryptPassword = await bcrypt.hash(password, 16);

    resetUser = user;
    resetUser.password = bcryptPassword;

    // REMOVING USER TOKEN FROM DATABASE AFTER SETTING NEW PASSWORD...
    resetUser.resetToken = undefined;
    resetUser.resetTokenExpiration = undefined;
    await resetUser.save();

    // AFTER SUCCESSFULLY RESETTING NEW PASSWORD, SEND EMAIL TO USER FOR SUCCESS MESSAGE ...
    transportor.sendMail({
      to: user.email,
      from: "mukundlalge49@gmail.com",
      subject: "Congratulations,reset successfully!",
      html: "<h1>You have successfully change your password!</h1>",
    });

    // REDIRECT USER AFTER SETTING NEW PASSWORD ...
    return {
      success: true,
      message: "Password reset successfully!",
    };
  } catch (error) {
    return error;
  }
};
exports.postLogOut = (args, request) => {
  try {
    request.session.destroy((error) => {
      if (error) {
        const err = new Error(error);
        err.httpStatusCode = 505;
        throw err;
      }
    });
    return { success: true, message: "Logout successfully!" };
  } catch (error) {
    return error;
  }
};
