const Product = require("../models/Product");

const getPagination = async (page, userId) => {
  const ITEM_PER_PAGE = 4;
  const currentPage = +page || 1;
  const startPage = (currentPage - 1) * ITEM_PER_PAGE;
  const endPage = currentPage * ITEM_PER_PAGE;
  let totalProducts = await Product.find().countDocuments();
  if (userId) {
    totalProducts = await Product.find({
      userId: userId
    }).countDocuments();
  }
  const totalPages = Math.ceil(totalProducts / ITEM_PER_PAGE);

  return {
    ITEM_PER_PAGE,
    currentPage,
    startPage,
    endPage,
    totalProducts,
    totalPages
  };
};
module.exports = getPagination;
