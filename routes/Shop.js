const express = require("express");
const orderController = require("../controllers/OrderControllers/OrderController");
const router = express.Router();

// @ROUTE           => /order/orderId
// @REQUEST_TYPE    => POST
// @DESC            => DELETING ORDER POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.delete("/:orderId", orderController.deleteOrder);

// @ROUTE           => /order/invoice
// @REQUEST_TYPE    => GET
// @DESC            => DOWNLOAD INVOICE FOR USER ...
// @ACCESS          => PRIVATE
router.post("/invoice", orderController.getInvoice);

module.exports = router;
