const express = require("express");
const adminController = require("../controllers/AdminControllers/AdminController");
const router = express.Router();
// @ROUTE           => /admin/add-product
// @REQUEST_TYPE    => POST
// @DESC            => ADD PRODUCT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/add-product", adminController.postAddProduct);

// @ROUTE           => /admin/edit-product
// @REQUEST_TYPE    => POST
// @DESC            => EDIT PRODUCT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/edit-product", adminController.postEditProduct);

module.exports = router;
