import axios from "axios";

const ShoppingCard = axios.create({
  baseURL: "https://shopping-king100.herokuapp.com",
  // baseURL: "http://localhost:5000/",
});
export default ShoppingCard;
