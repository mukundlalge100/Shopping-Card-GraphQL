import axios from "axios";

export const updateObject = (oldObject, newObject) => {
  return {
    ...oldObject,
    ...newObject
  };
};

export const setAuthToken = token => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export const verifyFile = files => {
  const fileTypes = [
    "image/x-png",
    "image/png",
    "image/jpg",
    "image/jpeg",
    "image/gif"
  ];
  const maxFileSize = 5e6;

  if (files && files.length > 0) {
    const currentFile = files[0];
    const fileSize = currentFile.size;
    const fileType = currentFile.type;

    if (fileSize > maxFileSize) {
      alert("File size is too large,should be less than 5mb");
      return false;
    }
    if (!fileTypes.includes(fileType)) {
      alert("This file type is not allowed ,please upload only images files");
      return false;
    }
    return true;
  }
};

export const isEmpty = value =>
  value === undefined ||
  value === "undefined" ||
  value === null ||
  (typeof value === "object" && Object.keys(value).length === 0) ||
  (typeof value === "string" && value.trim().length === 0);
