import React, { Component } from "react";
import classes from "./Product.module.scss";
import utilClasses from "../../Util/Util.module.scss";
import { connect } from "react-redux";
import * as actions from "../../store/Actions/IndexAction";
import Loader from "../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../HOC/ErrorHandler/SomethingWentWrong";

class Product extends Component {
  componentDidMount = () => {
    if (this.props.match.params.productId) {
      this.props.onGetProduct(this.props.match.params.productId);
    }
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  render() {
    let product = {};
    let descriptionList = null;

    if (this.props.product) {
      product = this.props.product;
      const descriptionArray = product ? product.description.split(",") : [];

      descriptionList = descriptionArray.map(desc => {
        return (
          <li className={classes.Product_Description__Item} key={desc}>
            => {desc}
          </li>
        );
      });
    }

    if (this.props.shopLoading) {
      return (
        <main className={`${classes.Products} ${utilClasses.Loader__Centered}`}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.Product}>
        <h1
          className={`${utilClasses.Primary__Heading} ${classes.Product_ProductDetails__Heading}`}
        >
          {product.title}
        </h1>
        <hr />
        <div className={classes.Product_ProductDetails}>
          <img
            src={product.imageUrl}
            alt={product.title}
            className={classes.Product_ProductDetails__Image}
          />
          <h2 className={utilClasses.Secondary__Heading}>
            &#8377;{product.price}
          </h2>
          <div className={classes.Product_ProductDetails__Description}>
            <h3 className={classes.Product_ProductDetails__Description_Heading}>
              Description of Product
            </h3>
            <ul className={classes.Product_Description}>{descriptionList}</ul>
          </div>
        </div>
        <hr style={{ marginBottom: "3rem" }} />
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    product: state.shopReducer.product,
    shopLoading: state.shopReducer.shopLoading,
    somethingWentWrong: state.shopReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: productId => dispatch(actions.getProduct(productId)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
