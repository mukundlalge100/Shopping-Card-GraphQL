import React, { Component } from "react";
import utilClasses from "../../../Util/Util.module.scss";
import classes from "./Add-Product.module.scss";
import renderTextAreaInput from "../../../Components/Redux-Form/Renderers/RenderTextAreaInput";
import { connect } from "react-redux";
import Loader from "../../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import * as actions from "../../../store/Actions/IndexAction";
import { reduxForm, Field, Form } from "redux-form";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import Dropzone from "react-dropzone";
import addProductValidations from "../../../Components/Redux-Form/Validators/AddProductValidations";
import { verifyFile } from "../../../Util/Util";
class AddProduct extends Component {
  state = {
    productFile: null
  };
  fileHandler = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      verifyFile(rejectedFiles);
    }
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        this.setState({ productFile: files[0] });
      }
    }
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  //ADD PRODUCT FORM SUBMIT HANDLER METHOD ...
  addProductFormSubmitHandler = formValues => {
    const formData = new FormData();

    formData.append("title", formValues.title);
    formData.append("price", formValues.price);
    formData.append("description", formValues.description);
    formData.append("imageUrl", this.state.productFile);
    // CALLING ADD PRODUCT METHOD IN REDUX STORE...
    this.props.onAddProduct(formData, this.props.history);
  };

  render() {
    const errors = { ...this.props.shopErrors };

    if (this.props.shopLoading) {
      // RENDERING LOADER ON SCREEN WHEN LOGIN FORM IS SUBMITED UNTIL SUCCESS...
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.AddProduct}>
        <h1 className={utilClasses.Primary__Heading}>
          Product Details Form...
        </h1>
        <Form
          onSubmit={this.props.handleSubmit(this.addProductFormSubmitHandler)}
          className={classes.AddProduct_Form}
        >
          <Field
            label="Enter Title"
            name="title"
            component={renderInput}
            className="Input"
            placeholder="Enter Title"
            error={errors.titleIsNotValid}
          />
          {this.state.productFile !== null ? (
            <section className={classes.AddProduct_Dropzone}>
              <div className={classes.AddProduct_Dropzone__Input}>
                <label htmlFor="imageUrl">{this.state.productFile.name}</label>
              </div>
            </section>
          ) : (
            <Dropzone
              onDrop={this.fileHandler}
              multiple={false}
              accept="image/*"
            >
              {({ getRootProps, getInputProps }) => {
                return (
                  <section className={classes.AddProduct_Dropzone}>
                    <div
                      {...getRootProps()}
                      className={classes.AddProduct_Dropzone__Input}
                    >
                      <label htmlFor="imageUrl">
                        Drag file or click to select files =>
                      </label>
                      <input
                        name="imageUrl"
                        {...getInputProps()}
                        className={classes.AddProduct_Dropzone__Input_Input}
                      />
                    </div>
                  </section>
                );
              }}
            </Dropzone>
          )}
          <Field
            type="number"
            name="price"
            component={renderInput}
            id="price"
            placeholder="Enter Price(In Dollars Only)"
            label="Enter Price(In Dollars Only)"
            error={errors.priceIsNotValid}
          />
          <Field
            component={renderTextAreaInput}
            label="Enter Description about product"
            info="Seperate each feature or description of product by comma(,)"
            name="description"
            placeholder="Enter Description about product"
            error={errors.descriptionIsNotValid}
          />
          <button
            className={`${utilClasses.Button} ${classes.AddProduct_Form__Button}`}
          >
            Add-Product
          </button>
        </Form>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    shopLoading: state.shopReducer.shopLoading,
    shopErrors: state.shopReducer.shopErrors,
    somethingWentWrong: state.shopReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAddProduct: (formData, history) =>
      dispatch(actions.addProduct(formData, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler())
  };
};

AddProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProduct);

export default reduxForm({
  form: "addProductForm",
  validate: addProductValidations
})(AddProduct);
