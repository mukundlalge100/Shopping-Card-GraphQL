import React, { Component } from "react";
import utilClasses from "../../../Util/Util.module.scss";
import classes from "./Edit-Product.module.scss";
import renderTextAreaInput from "../../../Components/Redux-Form/Renderers/RenderTextAreaInput";
import { connect } from "react-redux";
import Loader from "../../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../../HOC/ErrorHandler/SomethingWentWrong";
import * as actions from "../../../store/Actions/IndexAction";
import { reduxForm, Field, Form } from "redux-form";
import renderInput from "../../../Components/Redux-Form/Renderers/RenderInput";
import Dropzone from "react-dropzone";
import addProductValidations from "../../../Components/Redux-Form/Validators/AddProductValidations";
import { verifyFile } from "../../../Util/Util";

class EditProduct extends Component {
  state = {
    productFile: null
  };

  componentDidMount = () => {
    if (this.props.match.params.productId) {
      this.props.onGetProduct(this.props.match.params.productId);
    }
  };

  fileHandler = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      verifyFile(rejectedFiles);
    }
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        this.setState({ productFile: files[0] });
      }
    }
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  //EDIT PRODUCT FORM SUBMIT HANDLER METHOD ...
  editProductFormSubmitHandler = formValues => {
    const formData = new FormData();

    if (this.state.productFile !== null) {
      formData.append("imageUrl", this.state.productFile);
    }
    formData.append("title", formValues.title);
    formData.append("price", formValues.price);
    formData.append("description", formValues.description);
    formData.append("productId", this.props.product._id);

    this.props.onEditProduct(formData, this.props.history);
  };

  render() {
    const errors = { ...this.props.shopErrors };

    if (this.props.shopLoading) {
      // RENDERING LOADER ON SCREEN WHEN LOGIN FORM IS SUBMITED UNTIL SUCCESS...
      return (
        <main className={utilClasses.Loader__Centered}>
          <Loader />
        </main>
      );
    }
    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <main className={classes.AddProduct}>
        <h1 className={utilClasses.Primary__Heading}>
          Product Edit details Form...
        </h1>
        <Form
          onSubmit={this.props.handleSubmit(this.editProductFormSubmitHandler)}
          className={classes.AddProduct_Form}
        >
          <Field
            label="Enter Title"
            name="title"
            component={renderInput}
            className="Input"
            placeholder="Enter Title"
            error={errors.titleIsNotValid}
          />
          {this.state.productFile !== null ? (
            <section className={classes.AddProduct_Dropzone}>
              <div className={classes.AddProduct_Dropzone__Input}>
                <label htmlFor="imageUrl">{this.state.productFile.name}</label>
              </div>
            </section>
          ) : (
            <Dropzone
              onDrop={this.fileHandler}
              multiple={false}
              accept="image/*"
            >
              {({ getRootProps, getInputProps }) => {
                return (
                  <section className={classes.AddProduct_Dropzone}>
                    <div
                      {...getRootProps()}
                      className={classes.AddProduct_Dropzone__Input}
                    >
                      <label htmlFor="imageUrl">
                        Drag file or click to select files =>
                      </label>
                      <input
                        name="imageUrl"
                        {...getInputProps()}
                        className={classes.AddProduct_Dropzone__Input_Input}
                      />
                    </div>
                  </section>
                );
              }}
            </Dropzone>
          )}

          <Field
            name="price"
            type="number"
            component={renderInput}
            id="price"
            placeholder="Enter Price(In Dollars Only)"
            label="Enter Price(In Dollars Only)"
            error={errors.priceIsNotValid}
          />
          <Field
            component={renderTextAreaInput}
            label="Enter Description about product"
            name="description"
            placeholder="Enter Description about product"
            error={errors.descriptionIsNotValid}
          />
          <button
            className={`${utilClasses.Button} ${classes.AddProduct_Form__Button}`}
          >
            Edit-Product
          </button>
        </Form>
      </main>
    );
  }
}

const mapStateToProps = state => {
  let initialValues = null;
  if (state.shopReducer.product) {
    initialValues = {
      title: state.shopReducer.product.title,
      price: state.shopReducer.product.price,
      description: state.shopReducer.product.description
    };
  }
  return {
    initialValues: initialValues,
    shopLoading: state.shopReducer.shopLoading,
    shopErrors: state.shopReducer.shopErrors,
    product: state.shopReducer.product,
    somethingWentWrong: state.shopReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onEditProduct: (formData, history) =>
      dispatch(actions.editProduct(formData, history)),
    onGetProduct: productId => dispatch(actions.getProduct(productId)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler())
  };
};

EditProduct = reduxForm({
  form: "editProductForm",
  enableReinitialize: true,
  validate: addProductValidations
})(EditProduct);

EditProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProduct);

export default EditProduct;
