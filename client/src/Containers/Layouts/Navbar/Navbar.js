import React, { Component } from "react";
import classes from "./Navbar.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../../store/Actions/IndexAction";

import { withRouter } from "react-router-dom";
import SideDrawerToggle from "../../../Components/UI/SideDrawer/SideDrawerToggle/SideDrawerToggle";
import SideDrawer from "../../../Components/UI/SideDrawer/SideDrawer";
import { ReactComponent as SignUpSVG } from "../../../assets/SVG/sign-up-key.svg";
import { ReactComponent as LogInSVG } from "../../../assets/SVG/lock.svg";
import { ReactComponent as ShoppingCart } from "../../../assets/SVG/shopping-cart.svg";
import { ReactComponent as ClipBoard } from "../../../assets/SVG/clipboard.svg";
import { ReactComponent as AddProduct } from "../../../assets/SVG/add_shopping_cart.svg";
import { ReactComponent as Admin } from "../../../assets/SVG/admin.svg";
import { ReactComponent as LogOutSVG } from "../../../assets/SVG/unlocked.svg";
import { ReactComponent as Shopify } from "../../../assets/SVG/shopify.svg";

class Navbar extends Component {
  state = {
    showHideSideDrawer: false
  };
  authLogOut = () => {
    this.sideDrawerCloseHandler();
    this.props.onAuthLogOut(this.props.history);
  };
  showHideSideDrawer = () => {
    this.setState(prevState => {
      return {
        showHideSideDrawer: !prevState.showHideSideDrawer
      };
    });
  };
  sideDrawerCloseHandler = () => {
    this.setState({ showHideSideDrawer: false });
  };
  render() {
    let conditionalRoutes;
    if (this.props.isAuthenticated) {
      conditionalRoutes = (
        <div className={classes.Navbar_Nav__LinkContainer}>
          <Link
            to="/cart"
            className={classes.Navbar_Nav__Link}
            onClick={this.sideDrawerCloseHandler}
          >
            <ShoppingCart className={classes.Navbar_Nav__NavbarSVG} />
            Cart
          </Link>
          <Link
            to="/orders"
            className={classes.Navbar_Nav__Link}
            onClick={this.sideDrawerCloseHandler}
          >
            <ClipBoard className={classes.Navbar_Nav__NavbarSVG} />
            Orders
          </Link>
          <Link
            to="/admin/add-product"
            className={classes.Navbar_Nav__Link}
            onClick={this.sideDrawerCloseHandler}
          >
            <AddProduct className={classes.Navbar_Nav__NavbarSVG} />
            Add Product
          </Link>
          <Link
            to="/admin/products"
            className={classes.Navbar_Nav__Link}
            onClick={this.sideDrawerCloseHandler}
          >
            <Admin className={classes.Navbar_Nav__NavbarSVG} />
            Admin Products
          </Link>
          <div className={classes.Navbar_Nav__Link} onClick={this.authLogOut}>
            <LogOutSVG className={classes.Navbar_Nav__NavbarSVG} />
            LogOut
          </div>
        </div>
      );
    } else {
      conditionalRoutes = (
        <div className={classes.Navbar_Nav__LinkContainer}>
          <Link
            className={classes.Navbar_Nav__Link}
            to="/signup"
            onClick={this.sideDrawerCloseHandler}
          >
            <SignUpSVG className={classes.Navbar_Nav__NavbarSVG} />
            SignUp
          </Link>

          <Link
            className={classes.Navbar_Nav__Link}
            to="/login"
            onClick={this.sideDrawerCloseHandler}
          >
            <LogInSVG className={classes.Navbar_Nav__NavbarSVG} />
            LogIn
          </Link>
        </div>
      );
    }
    return (
      <main className={classes.Navbar}>
        <SideDrawerToggle sideDrawerToggle={this.showHideSideDrawer} />

        {this.state.showHideSideDrawer ? (
          <SideDrawer
            showHideSideDrawer={this.state.showHideSideDrawer}
            sideDrawerClosedHandler={this.sideDrawerCloseHandler}
          >
            <nav
              className={`${classes.Navbar_Nav} ${
                classes.Navbar_Nav__SideDrawerShow
              }`}
            >
              <Link
                className={`${classes.Navbar_Nav__Link} ${
                  classes.Navbar_Nav__Link_Brand
                }`}
                to="/"
                onClick={this.sideDrawerCloseHandler}
              >
                <Shopify className={classes.Navbar_Nav__BrandSVG} />
                Shop
              </Link>
              {conditionalRoutes}
            </nav>
          </SideDrawer>
        ) : (
          <nav
            className={`${classes.Navbar_Nav} ${
              classes.Navbar_Nav__SideDrawerHide
            }`}
          >
            <Link
              className={`${classes.Navbar_Nav__Link} ${
                classes.Navbar_Nav__Link_Brand
              }`}
              to="/"
            >
              <Shopify className={classes.Navbar_Nav__BrandSVG} />
              Shop
            </Link>
            {conditionalRoutes}
          </nav>
        )}
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    user: state.authReducer.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuthLogOut: history => dispatch(actions.authLogOut(history))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Navbar));
