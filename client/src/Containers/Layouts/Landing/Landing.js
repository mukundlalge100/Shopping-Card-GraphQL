import React, { Component } from "react";
import classes from "./Landing.module.scss";
import UtilClasses from "../../../Util/Util.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

//LANGING PAGE COMPONENT ...
class Landing extends Component {
  render() {
    return (
      <div className={classes.Landing}>
        <div className={classes.Landing_Header}>
          <h1 className={UtilClasses.Primary__Heading}>Shopping King</h1>
          <p className={UtilClasses.Paragraph}>
            Upload your products,shop with shopping master and much more to
            shop.
          </p>
        </div>
        <div className={classes.Landing_Actions}>
          <Link
            to="/signup"
            className={`${UtilClasses.Button} ${classes.Landing_Button}`}
          >
            SignUp
          </Link>
          <Link
            to="/login"
            className={`${UtilClasses.Button} ${classes.Landing_Button}`}
          >
            LogIn
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated
  };
};
export default connect(
  mapStateToProps,
  null
)(Landing);
