import React, { Component } from "react";
import commonClasses from "../../Containers/ProductCard/ProductCard.module.scss";
import classes from "./CartProductCard.module.scss";
import utilClasses from "../../Util/Util.module.scss";
import { ReactComponent as Increment } from "../../assets/SVG/increement.svg";
import { ReactComponent as Decrement } from "../../assets/SVG/decrement.svg";
import * as actions from "../../store/Actions/IndexAction";
import { connect } from "react-redux";

class CartProductCard extends Component {
  state = {
    product: {}
  };
  componentDidMount = () => {
    this.setState({ product: this.props.product });
  };
  deleteProductFromCart = () => {
    this.props.onDeleteProductFromCart(this.props.product.productId._id);
  };

  decrementQuantity = () => {
    let product = { ...this.state.product };
    product.quantity -= 1;
    if (product.quantity < 1) {
      return;
    } else {
      this.setState({ product });
      this.props.onDecrementQuantity(
        this.props.product.productId._id,
        this.state.product.quantity - 1
      );
    }
  };
  incrementQuantity = () => {
    let product = { ...this.state.product };
    product.quantity += 1;
    this.setState({ product });
    this.props.onIncrementQuantity(
      this.props.product.productId._id,
      this.state.product.quantity + 1
    );
  };
  render() {
    const { title, imageUrl, price } = this.props.product.productId;
    const backSide = (
      <div
        className={`${commonClasses.ProductCard_Side} ${commonClasses.ProductCard_Side__BackSide}`}
      >
        <div className={classes.CartProductCard_Quantity}>
          Total Quantity &rArr;
          <div className={classes.CartProductCard_Quantity__Container}>
            <Increment
              className={classes.CartProductCard_Icon}
              onClick={this.incrementQuantity}
            />
            {this.state.product.quantity}
            <Decrement
              className={classes.CartProductCard_Icon}
              onClick={this.decrementQuantity}
            />
          </div>
        </div>
        <button
          className={utilClasses.Button}
          onClick={this.deleteProductFromCart}
        >
          Delete
        </button>
      </div>
    );
    const frontSide = (
      <div
        className={`${commonClasses.ProductCard_Side} ${commonClasses.ProductCard_Side__FrontSide}`}
      >
        <header className={commonClasses.ProductCard_Header}>
          <h1 className={utilClasses.Tertiary__Heading}>{title}</h1>
        </header>

        <div className={commonClasses.ProductCard_ImageContainer}>
          <img
            src={imageUrl}
            alt="A Book"
            className={commonClasses.ProductCard_ImageContainer__Image}
          />
        </div>
        <div
          className={commonClasses.ProductCard_Price}
          style={{ textAlign: "center" }}
        >
          &#8377;{price}
        </div>
      </div>
    );
    return (
      <article className={commonClasses.ProductCard}>
        {frontSide}
        {backSide}
      </article>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onDeleteProductFromCart: productId =>
      dispatch(actions.deleteProductFromCart(productId)),
    onIncrementQuantity: (productId, quantity) =>
      dispatch(actions.incrementQuantity(productId, quantity)),
    onDecrementQuantity: (productId, quantity) =>
      dispatch(actions.decrementQuantity(productId, quantity))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartProductCard);
