import React, { Component } from "react";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";
import classes from "./Products.module.scss";
import utilClasses from "../../Util/Util.module.scss";
import * as actions from "../../store/Actions/IndexAction";
import ProductCard from "../ProductCard/ProductCard";
import { ReactComponent as Shopify } from "../../assets/SVG/shopify.svg";
import Auxilary from "../../HOC/Auxiliary/Auxiliary";
import Loader from "../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../HOC/ErrorHandler/SomethingWentWrong";

class Products extends Component {
  state = {
    currentPage: 1
  };
  componentDidMount = () => {
    this.props.onGetProducts(this.state.currentPage);
    this.props.onChangeEditingStatus(false);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.currentPage !== this.state.currentPage) {
      this.props.onGetProducts(this.state.currentPage);
    }
  };

  changePageHandler = currentPage => {
    this.setState({ currentPage });
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  render() {
    let products = null;

    if (this.props.products.length > 0) {
      products = this.props.products.map(product => {
        return (
          <ProductCard
            changePageHandler={this.changePageHandler}
            key={product._id}
            product={product}
            history={this.props.history}
          />
        );
      });
    }

    if (this.props.shopLoading) {
      return (
        <main className={`${classes.Products} ${utilClasses.Loader__Centered}`}>
          <Loader />
        </main>
      );
    }

    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }

    return (
      <Auxilary>
        {this.props.products.length > 0 ? (
          <main className={classes.Products}>
            <div className={classes.Products_Header}>
              <Shopify className={classes.Products_Header__Brand} />
              <h1
                className={`${utilClasses.Primary__Heading} ${utilClasses.Centered} ${classes.Products_Header__Heading}`}
              >
                Shopping King
              </h1>
            </div>
            <div className={classes.Products_Container}>{products}</div>
            {this.props.totalItemsCount > 4 ? (
              <Pagination
                pageRangeDisplayed={3}
                onChange={this.changePageHandler}
                innerClass={utilClasses.Pagination}
                itemsCountPerPage={this.props.itemsCountPerPage}
                activePage={this.state.currentPage}
                totalItemsCount={this.props.totalItemsCount}
                activeLinkClass={
                  utilClasses.Pagination_LinkContainer__Link_Active
                }
                activeClass={utilClasses.Pagination_LinkContainer__Active}
                itemClass={utilClasses.Pagination_LinkContainer}
                linkClass={utilClasses.Pagination_LinkContainer__Link}
              />
            ) : null}
          </main>
        ) : (
          <main className={classes.Products}>
            <h1 className={utilClasses.Primary__Heading}>
              No Products are Added!
            </h1>
          </main>
        )}
      </Auxilary>
    );
  }
}
const mapStateToProps = state => {
  return {
    products: state.shopReducer.products,
    isEditing: state.shopReducer.isEditing,
    shopLoading: state.shopReducer.shopLoading,
    somethingWentWrong: state.shopReducer.somethingWentWrong,
    itemsCountPerPage: state.shopReducer.itemsCountPerPage,
    totalItemsCount: state.shopReducer.totalItemsCount,
    isAuthenticated: state.authReducer.isAuthenticated
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProducts: currentpage => dispatch(actions.getProducts(currentpage)),
    onChangeEditingStatus: isEditing =>
      dispatch(actions.changeEditingStatus(isEditing)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
