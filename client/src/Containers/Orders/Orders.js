import React, { Component } from "react";
import utilClasses from "../../Util/Util.module.scss";
import classes from "./Orders.module.scss";
import { connect } from "react-redux";
import * as actions from "../../store/Actions/IndexAction";
import Order from "../../Components/Order/Order";
import SomethingWentWrong from "../../HOC/ErrorHandler/SomethingWentWrong";
import Loader from "../../Components/UI/Loader/Loader";
import Auxilary from "../../HOC/Auxiliary/Auxiliary";

class Orders extends Component {
  componentDidMount = () => {
    this.props.onGetOrders();
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };
  deleteOrder = orderId => {
    this.props.onDeleteOrder(orderId);
  };
  getInvoice = orderId => {
    this.props.OnGetInvoice(orderId);
  };
  render() {
    let orders = null;
    if (this.props.orders) {
      if (this.props.orders.length > 0) {
        orders = this.props.orders.map(order => {
          return (
            <Order
              key={order._id}
              order={order}
              deleteOrder={this.deleteOrder}
              getInvoice={this.getInvoice}
            />
          );
        });
      }
    }

    if (this.props.shopLoading) {
      return (
        <main className={`${classes.Products} ${utilClasses.Loader__Centered}`}>
          <Loader />
        </main>
      );
    }

    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    return (
      <Auxilary>
        {this.props.orders.length > 0 ? (
          <div style={{ paddingTop: "3rem", backgroundColor: "#f7f7f7" }}>
            <h1
              className={utilClasses.Primary__Heading}
              style={{ textAlign: "center" }}
            >
              My Orders
            </h1>
            <main className={classes.Orders}>{orders}</main>
          </div>
        ) : (
          <main className={classes.Orders}>
            <h1 className="Primary__Heading">No orders are Available!</h1>
          </main>
        )}
      </Auxilary>
    );
  }
}

const mapStateToProps = state => {
  return {
    orders: state.shopReducer.orders,
    shopLoading: state.shopReducer.shopLoading,
    somethingWentWrong: state.shopReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetOrders: () => dispatch(actions.getOrders()),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler()),
    OnGetInvoice: orderId => dispatch(actions.getInvoice(orderId)),
    onDeleteOrder: orderId => dispatch(actions.deleteOrder(orderId))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders);
