import React, { Component } from "react";
import classes from "./ProductCard.module.scss";
import utilClasses from "../../Util/Util.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as actions from "../../store/Actions/IndexAction";

class ProductCard extends Component {
  deleteProduct = (productId, currentPage) => {
    if (this.props.products.length === 1) {
      this.props.changePageHandler(currentPage - 1);
      this.props.onDeleteProduct(productId, currentPage - 1);
    } else {
      this.props.onDeleteProduct(productId, currentPage);
    }
  };

  addProductToCart = (productId) => {
    this.props.onAddProductToCart(productId, this.props.history);
  };

  render() {
    const { title, price, description, imageUrl, _id } = this.props.product;

    const descriptionArray = description.split(",");

    const descriptionList = descriptionArray.map((des) => {
      return <li>{des}</li>;
    });
    const backSide = (
      <div
        className={`${classes.ProductCard_Side} ${classes.ProductCard_Side__BackSide}`}
      >
        <h2
          className={`${utilClasses.Secondary__Heading} ${classes.ProductCard_Only}`}
        >
          Only
        </h2>
        <div className={classes.ProductCard_Price}>&#36; {price}</div>
        {this.props.isEditing ? (
          <div className={classes.ProductCard_Actions}>
            <button
              onClick={() => this.deleteProduct(_id, this.props.currentPage)}
              className={`${utilClasses.Button} ${classes.ProductCard_Actions__Button}`}
            >
              Delete
            </button>
            <Link
              to={`/admin/edit-product/${_id}`}
              className={`${utilClasses.Button} ${classes.ProductCard_Actions__Button}`}
            >
              Edit Product
            </Link>
          </div>
        ) : (
          <div className={classes.ProductCard_Actions}>
            <Link
              to={`/products/${_id}`}
              className={`${utilClasses.Button} ${classes.ProductCard_Actions__Button}`}
            >
              Details
            </Link>
            {this.props.isAuthenticated ? (
              <button
                className={`${utilClasses.Button}`}
                onClick={() => this.addProductToCart(_id)}
              >
                Add to Cart
              </button>
            ) : null}
          </div>
        )}
      </div>
    );
    const frontSide = (
      <div
        className={`${classes.ProductCard_Side} ${classes.ProductCard_Side__FrontSide}`}
      >
        <header className={classes.ProductCard_Header}>
          <h1 className={classes.ProductCard_Header__Heading}>{title}</h1>
        </header>

        <div className={classes.ProductCard_ImageContainer}>
          <img
            src={imageUrl}
            alt="A Book"
            className={classes.ProductCard_ImageContainer__Image}
          />
        </div>

        <ul className={classes.ProductCard_Description}>
          {descriptionList.length > 0 ? descriptionList : description}
        </ul>
      </div>
    );
    return (
      <article className={classes.ProductCard}>
        {frontSide}
        {backSide}
      </article>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isEditing: state.shopReducer.isEditing,
    products: state.shopReducer.products,
    isAuthenticated: state.authReducer.isAuthenticated,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteProduct: (productId, currentPage) =>
      dispatch(actions.deleteProduct(productId, currentPage)),
    onAddProductToCart: (productId, history) =>
      dispatch(actions.addProductToCart(productId, history)),
  };
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
  products: PropTypes.array.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onDeleteProduct: PropTypes.func.isRequired,
  changePageHandler: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);
