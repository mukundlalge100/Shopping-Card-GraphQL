import React, { Component } from "react";
import utilClasses from "../../Util/Util.module.scss";
import classes from "./Checkout.module.scss";
import { connect } from "react-redux";
import * as actions from "../../store/Actions/IndexAction";
import Loader from "../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../HOC/ErrorHandler/SomethingWentWrong";
import StripeCheckout from "react-stripe-checkout";

class Checkout extends Component {
  componentDidMount = () => {
    this.props.onGetCheckoutDetails();
  };
  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  onSubmit = (token) => {
    this.props.onCreateOrder(token, this.props.history);
  };

  render() {
    if (this.props.shopLoading) {
      return (
        <main className={`${classes.Products} ${utilClasses.Loader__Centered}`}>
          <Loader />
        </main>
      );
    }

    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }
    const products = this.props.cartProducts.map((product) => {
      return (
        <div className={classes.Checkout_Product} key={product._id}>
          <h2
            className={utilClasses.Secondary__Heading}
            style={{ color: "#fff" }}
          >
            Product Name &rArr; {product.productId.title}
          </h2>
          <h2
            className={utilClasses.Secondary__Heading}
            style={{ color: "#fff" }}
          >
            Quantity &rArr;{product.quantity}
          </h2>
        </div>
      );
    });
    return (
      <main className={classes.Checkout}>
        <h1
          className={`${utilClasses.Primary__Heading} ${utilClasses.Margin__Top_Medium}`}
        >
          Checkout!
        </h1>
        {products}
        <h2 className={utilClasses.Primary__Heading}>
          TotalPrice &rArr; Rs. {this.props.totalPrice}
        </h2>
        <StripeCheckout
          allowRememberMe={false}
          className={classes.Checkout_Stripe}
          token={this.onSubmit}
          amount={this.props.totalPrice * 100}
          currency="INR"
          panelLabel="Pay"
          description="Buy Product with shopping Master"
          label="Go To Payment"
          name="Shopping Master"
          stripeKey="pk_test_f4pIJz1GdowAlr4sEmfVwH1500aD1a8aiL"
          billingAddress={false}
        />
      </main>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cartProducts: state.shopReducer.cartProducts,
    totalPrice: state.shopReducer.totalPrice,
    shopLoading: state.shopReducer.shopLoading,
    somethingWentWrong: state.shopReducer.somethingWentWrong,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onGetCheckoutDetails: () => dispatch(actions.getCheckoutDetails()),
    onCreateOrder: (token, history) =>
      dispatch(actions.createOrder(token, history)),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
