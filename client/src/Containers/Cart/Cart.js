import React, { Component } from "react";
import utilClasses from "../../Util/Util.module.scss";
import classes from "./Cart.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "../../Components/UI/Loader/Loader";
import SomethingWentWrong from "../../HOC/ErrorHandler/SomethingWentWrong";
import * as actions from "../../store/Actions/IndexAction";
import CartProductCard from "../CartProductCard/CartProductCard";

class Cart extends Component {
  state = {
    quantity: 1
  };

  componentDidMount = () => {
    this.props.onGetCartProducts();
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  render() {
    if (this.props.shopLoading) {
      return (
        <main className={`${classes.Products} ${utilClasses.Loader__Centered}`}>
          <Loader />
        </main>
      );
    }

    if (this.props.somethingWentWrong) {
      return (
        <SomethingWentWrong
          showModal={this.props.somethingWentWrong ? true : false}
          somethingWentWrong={this.props.somethingWentWrong}
          somethingWentWrongCloseHandler={this.somethingWentWrongCloseHandler}
        />
      );
    }

    const products = this.props.cartProducts.map(product => {
      return (
        <CartProductCard
          product={product}
          key={product._id}
          deleteProductFromCart={this.deleteProductFromCart}
        />
      );
    });

    if (this.props.cartProducts.length > 0) {
      return (
        <main className={classes.Cart}>
          <h1
            className={utilClasses.Primary__Heading}
            style={{ textAlign: "center" }}
          >
            My Cart..
          </h1>
          <div className={classes.Cart_Container}>{products}</div>
          <div className={classes.Cart_Link}>
            <Link to="/checkout" className={utilClasses.Button}>
              Order Now!
            </Link>
          </div>
        </main>
      );
    } else {
      return (
        <main className={classes.Cart}>
          <h1 className={utilClasses.Primary__Heading}>
            No products in cart...
          </h1>
        </main>
      );
    }
  }
}
const mapStateToProps = state => {
  return {
    cartProducts: state.shopReducer.cartProducts,
    shopLoading: state.shopReducer.shopLoading,
    somethingWentWrong: state.shopReducer.somethingWentWrong
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCartProducts: () => dispatch(actions.getCartProducts()),
    onSomethingWentWrongClose: () =>
      dispatch(actions.shopSomethingWentWrongCloseHandler())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
