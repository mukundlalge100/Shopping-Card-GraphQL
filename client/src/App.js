import React, { Component, Suspense, lazy } from "react";
import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "./Components/UI/Loader/Loader";
import Navbar from "./Containers/Layouts/Navbar/Navbar";
import Footer from "./Components/Layouts/Footer/Footer";
import Landing from "./Containers/Layouts/Landing/Landing";
import * as actions from "./store/Actions/IndexAction";
//STYLLING IMPORTS
import classes from "./App.module.scss";
import EditProduct from "./Containers/Product/Edit-Product/Edit-Product";
import Cart from "./Containers/Cart/Cart";
import Checkout from "./Containers/Checkout/Checkout";
import Orders from "./Containers/Orders/Orders";
import PageNotFound from "./Components/PageNotFound/PageNotFound";

const LogIn = lazy(() => import("./Containers/Auth/LogIn/LogIn"));
const SignUp = lazy(() => import("./Containers/Auth/SignUp/SignUp"));
const Products = lazy(() => import("./Containers/Products/Products"));
const AdminProducts = lazy(() =>
  import("./Containers/AdminProducts/AdminProducts")
);
const Product = lazy(() => import("./Containers/Product/Product"));
const AddProduct = lazy(() =>
  import("./Containers/Product/Add-Product/Add-Product")
);
const NewPassword = lazy(() =>
  import("./Containers/Auth/NewPassword/NewPassword")
);

const ResetPassword = lazy(() =>
  import("./Containers/Auth/ResetPassword/ResetPassword")
);

// ASYNC LAZY COMPONENTS CHUNKS ...

class App extends Component {
  componentDidMount = () => {
    this.props.onAuthTryToLogIn(this.props.history);
  };
  render() {
    let routes;
    if (this.props.isAuthenticated) {
      routes = (
        // PRIVATE ROUTES ...
        <Switch>
          <Route exact path="/" component={Products} />
          <Route exact path="/admin/add-product" component={AddProduct} />
          <Route exact path="/admin/products" component={AdminProducts} />
          <Route exact path="/products/:productId" component={Product} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/checkout" component={Checkout} />
          <Route exact path="/orders" component={Orders} />
          <Route
            exact
            path="/admin/edit-product/:productId"
            component={EditProduct}
          />

          <Route exact path="/pageNotFound" component={PageNotFound} />
          <Redirect exact to="/pageNotFound" />
        </Switch>
      );
    } else {
      routes = (
        // PUBLIC ROUTES ...
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/login" component={LogIn} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/reset-password/:token" component={NewPassword} />
          <Route exact path="/reset-password" component={ResetPassword} />
          <Route exact path="/pageNotFound" component={PageNotFound} />
          <Redirect exact to="/pageNotFound" />
        </Switch>
      );
    }
    return (
      <main className={classes.App}>
        <Suspense
          fallback={
            <div className={classes.App_Loader}>
              <Loader />
            </div>
          }
        >
          <Navbar />
          {routes}
          <Footer />
        </Suspense>
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuthTryToLogIn: history => dispatch(actions.authCheckLogInState(history))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(App));
