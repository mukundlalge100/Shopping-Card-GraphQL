const shopQueries = {
  getProductsQuery: `
    query {
      getProducts{
        products {
          _id
          title
          imageUrl
          price
          description
        }
        itemsCountPerPage
        totalItemsCount
      }
    }
  `,
  getAdminProductsQuery: `
    query {
      getAdminProducts {
        products {
          _id
          title
          imageUrl
          price
          description
        }
        itemsCountPerPage 
        totalItemsCount
      }
    }
  `,
  getCartProductsQuery: `
    query {
      getCartProducts {
        products {
          _id
          productId {
            _id
            title
            imageUrl
            price
            description
          }
          quantity
        }
      }
    }
  `,
  getProductQuery: `
    query {
      getProduct{
        _id
        title
        imageUrl
        price
        description
      }
    }
  `,
  getCheckoutDetailsQuery: `
    query {
      getCheckoutDetails {
        products {
           _id
          productId {
            _id
            title
            imageUrl
            price
            description
          }
          quantity
        }
        totalPrice 

      }
    }
  `,
  addProductToCartQuery: `
    mutation {
      addProductToCart {
        success
        message
      }
    }
  `,
  deleteProductQuery: `
    mutation {
      deleteProduct {
        success
        message
      }
    }
  `,
  deleteProductFromCartQuery: `
    mutation {
      deleteProductFromCart {
        success
        message
      }
    }
  `,
  incrementQuantityQuery: `
    mutation {
      incrementQuantity {
        success
        message
      }
    }
  `,
  decrementQuantityQuery: `
    mutation {
      decrementQuantity {
        success
        message
      }
    }
  `,
  createOrderQuery: `
    mutation {
      postOrder {
        success
        message
      }
    }
  `,
  getOrdersQuery: `
    query {
      getOrders {
        orders {
          _id
          products{
            product{
              _id
            }
            quantity
          }
          totalPrice
        }
      }
    }
  `,
  deleteOrderQuery: `
    mutation {
      deleteOrder {
        success
        message
      }
    }
  `
};
export default shopQueries;
