const AuthQueries = {
  logOutQuery: `
    query {
      logOut {
        success
        message
      }
    }
  `
};
export default AuthQueries;
