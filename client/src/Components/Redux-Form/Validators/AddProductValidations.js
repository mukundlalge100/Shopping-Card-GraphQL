import validator from "validator";
import { isEmpty } from "../../../Util/Util";
import { isNumber } from "util";

const addProductValidations = ({ title, price, description }) => {
  const errors = {};

  if (isEmpty(title)) {
    errors.title = "Title field is required!";
  } else if (!validator.isLength(title, { min: 4, max: 30 })) {
    errors.title = "Title must be in between 4 and 30 characters!";
  }

  if (isEmpty(price)) {
    errors.price = "Price field is required!";
  } else if (!isNumber(+price)) {
    errors.price = "Price is should numeric value!";
  }

  if (isEmpty(description)) {
    errors.description = "Description field is required!";
  } else if (!validator.isLength(description, { min: 10, max: 400 })) {
    errors.description =
      "Description must be in between 10 and 400 characters!";
  }
  return errors;
};
export default addProductValidations;
