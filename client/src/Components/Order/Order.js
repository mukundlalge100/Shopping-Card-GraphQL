import React from "react";
import utilClasses from "../../Util/Util.module.scss";
import classes from "./Order.module.scss";
import { ReactComponent as DeleteOrder } from "../../assets/SVG/cancel.svg";

const Order = props => {
  return (
    <div className={classes.Order}>
      <button
        className={classes.Order_DeleteButton}
        title="Delete Your Order Information"
        onClick={() => props.deleteOrder(props.order._id)}
      >
        <DeleteOrder className={classes.Order_DeleteButton__CloseIcon} />
      </button>
      <h1 className={utilClasses.Secondary__Heading}>
        Order No. &rArr; {props.order._id}
      </h1>
      <h3 className={utilClasses.Tertiary__Heading}>
        Please click Below invoice button to see invoice details!!!
      </h3>
      <button
        className={utilClasses.Button}
        onClick={() => props.getInvoice(props.order._id)}
      >
        Your Invoice
      </button>
    </div>
  );
};

export default Order;
