import { actionTypes } from "./ActionTypes";
import GraphQLQueries from "../../GraphQL/ShopQueries";
import axios from "../../api/Shopping-Card";

const shopSomethingWentWrong = somethingWentWrong => {
  return {
    type: actionTypes.SHOP_SOMETHING_WENT_WRONG,
    somethingWentWrong: somethingWentWrong
  };
};
export const shopSomethingWentWrongCloseHandler = () => {
  return {
    type: actionTypes.SHOP_SOMETHING_WENT_WRONG_CLOSE
  };
};
export const changeEditingStatus = isEditing => {
  return {
    type: actionTypes.CHANGE_EDITING_STATUS,
    isEditing
  };
};

const getProductsStart = () => {
  return {
    type: actionTypes.GET_PRODUCTS_START
  };
};
const getProductsSuccess = response => {
  return {
    type: actionTypes.GET_PRODUCTS_SUCCESS,
    response
  };
};
const getProductsFail = errors => {
  return {
    type: actionTypes.GET_PRODUCTS_FAIL,
    errors: errors
  };
};
export const getProducts = currentPage => {
  return async dispatch => {
    dispatch(getProductsStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getProductsQuery,
        currentPage: currentPage
      });
      dispatch(getProductsSuccess(response.data.data.getProducts));
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(getProductsFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

const getAdminProductsStart = () => {
  return {
    type: actionTypes.GET_ADMIN_PRODUCTS_START
  };
};
const getAdminProductsSuccess = response => {
  return {
    type: actionTypes.GET_ADMIN_PRODUCTS_SUCCESS,
    response
  };
};
const getAdminProductsFail = errors => {
  return {
    type: actionTypes.GET_ADMIN_PRODUCTS_FAIL,
    errors: errors
  };
};
export const getAdminProducts = currentPage => {
  return async dispatch => {
    dispatch(getAdminProductsStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getAdminProductsQuery,
        currentPage: currentPage
      });
      dispatch(getAdminProductsSuccess(response.data.data.getAdminProducts));
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(getAdminProductsFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// GET PRODUCT ACTIONS ...
const getProductStart = () => {
  return {
    type: actionTypes.GET_PRODUCT_START
  };
};
const getProductSuccess = response => {
  return {
    type: actionTypes.GET_PRODUCT_SUCCESS,
    response
  };
};
const getProductFail = errors => {
  return {
    type: actionTypes.GET_PRODUCT_FAIL,
    errors: errors
  };
};
export const getProduct = productId => {
  return async dispatch => {
    dispatch(getProductStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getProductQuery,
        productId
      });
      dispatch(getProductSuccess(response.data.data.getProduct));
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(getProductFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// GET CART PRODUCTS ACTIONS ...
const getCartProductsStart = () => {
  return {
    type: actionTypes.GET_CART_PRODUCTS_START
  };
};
const getCartProductsSuccess = response => {
  return {
    type: actionTypes.GET_CART_PRODUCTS_SUCCESS,
    response
  };
};
const getCartProductsFail = errors => {
  return {
    type: actionTypes.GET_CART_PRODUCTS_FAIL,
    errors: errors
  };
};
export const getCartProducts = () => {
  return async dispatch => {
    dispatch(getCartProductsStart());
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getCartProductsQuery
      });
      dispatch(getCartProductsSuccess(response.data.data.getCartProducts));
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(getCartProductsFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// ADD PRODUCT ACTIONS ...
const addProductStart = () => {
  return {
    type: actionTypes.ADD_PRODUCT_START
  };
};
const addProductSuccess = response => {
  return {
    type: actionTypes.ADD_PRODUCT_SUCCESS
  };
};
const addProductFail = errors => {
  return {
    type: actionTypes.ADD_PRODUCT_FAIL,
    errors: errors
  };
};
export const addProduct = (formData, history) => {
  return async dispatch => {
    try {
      dispatch(addProductStart());
      const response = await axios.post("/admin/add-product", formData);
      dispatch(addProductSuccess(response));
      history.push("/");
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(addProductFail(error.response.data));
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// ADD PRODUCT ACTIONS ...
const editProductStart = () => {
  return {
    type: actionTypes.EDIT_PRODUCT_START
  };
};
const editProductSuccess = response => {
  return {
    type: actionTypes.EDIT_PRODUCT_SUCCESS
  };
};
const editProductFail = errors => {
  return {
    type: actionTypes.EDIT_PRODUCT_FAIL,
    errors: errors
  };
};
export const editProduct = (formData, history) => {
  return async dispatch => {
    try {
      dispatch(editProductStart());
      const response = await axios.post("/admin/edit-product", formData);
      dispatch(editProductSuccess(response));
      history.push("/admin/products");
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(editProductFail(error.response.data));
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// ADD PRODUCT TO CART ACTIONS ...
const addProductToCartStart = () => {
  return {
    type: actionTypes.ADD_PRODUCT_TO_CART_START
  };
};
const addProductToCartSuccess = () => {
  return {
    type: actionTypes.ADD_PRODUCT_TO_CART_SUCCESS
  };
};
const addProductToCartFail = errors => {
  return {
    type: actionTypes.ADD_PRODUCT_TO_CART_FAIL,
    errors: errors
  };
};
export const addProductToCart = (productId, history) => {
  return async dispatch => {
    try {
      dispatch(addProductToCartStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.addProductToCartQuery,
        productId
      });
      if (response.data.data.addProductToCart.success) {
        dispatch(addProductToCartSuccess());
      }
      history.push("/cart");
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(addProductToCartFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// DELETE PRODUCT ...
export const deleteProduct = (productId, currentPage) => {
  return async dispatch => {
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.deleteProductQuery,
        productId
      });

      if (response.data.data.deleteProduct.success) {
        dispatch(getAdminProducts(currentPage));
      }
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};
// DELETE PRODUCT FROM CART...
export const deleteProductFromCart = productId => {
  return async dispatch => {
    try {
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.deleteProductFromCartQuery,
        productId
      });

      if (response.data.data.deleteProductFromCart.success) {
        dispatch(getCartProducts());
      }
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// GET CHECKOUT DETAILS ...
const getCheckoutDetailsStart = () => {
  return {
    type: actionTypes.GET_CHECKOUT_DETAILS_START
  };
};
const getCheckoutDetailsSuccess = response => {
  return {
    type: actionTypes.GET_CHECKOUT_DETAILS_SUCCESS,
    response
  };
};
const getCheckoutDetailsFail = errors => {
  return {
    type: actionTypes.GET_CHECKOUT_DETAILS_FAIL,
    errors: errors
  };
};
export const getCheckoutDetails = () => {
  return async dispatch => {
    try {
      dispatch(getCheckoutDetailsStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getCheckoutDetailsQuery
      });
      dispatch(
        getCheckoutDetailsSuccess(response.data.data.getCheckoutDetails)
      );
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(
            getCheckoutDetailsFail(error.response.data.errors[0].errors)
          );
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// INCREAMENT QUANTITY OF PRODUCT IN CART ...
export const incrementQuantity = (productId, quantity) => {
  return async dispatch => {
    try {
      await axios.post("/graphql", {
        query: GraphQLQueries.incrementQuantityQuery,
        quantity,
        productId
      });
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};
// DECREMENT QUANTITY OF PRODUCT IN CART ...
export const decrementQuantity = (productId, quantity) => {
  return async dispatch => {
    try {
      await axios.post("/graphql", {
        query: GraphQLQueries.decrementQuantityQuery,
        quantity,
        productId
      });
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

//CREATE ORDER ...
const createOrderStart = () => {
  return {
    type: actionTypes.CREATE_ORDER_START
  };
};
const createOrderSuccess = response => {
  return {
    type: actionTypes.CREATE_ORDER_SUCCESS,
    response
  };
};
const createOrderFail = errors => {
  return {
    type: actionTypes.CREATE_ORDER_FAIL,
    errors: errors
  };
};
export const createOrder = (token, history) => {
  return async dispatch => {
    try {
      dispatch(createOrderStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.createOrderQuery,
        token
      });
      dispatch(createOrderSuccess(response.data.data.postOrder));
      history.push("/orders");
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(createOrderFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// GET ORDERS ...
const getOrdersStart = () => {
  return {
    type: actionTypes.GET_ORDERS_START
  };
};
const getOrdersSuccess = response => {
  return {
    type: actionTypes.GET_ORDERS_SUCCESS,
    response
  };
};
const getOrdersFail = errors => {
  return {
    type: actionTypes.GET_ORDERS_FAIL,
    errors: errors
  };
};
export const getOrders = () => {
  return async dispatch => {
    try {
      dispatch(getOrdersStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.getOrdersQuery
      });
      dispatch(getOrdersSuccess(response.data.data.getOrders));
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(getOrdersFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// GET INVOICE OF ORDER ...
export const getInvoice = orderId => {
  return async dispatch => {
    try {
      const response = await axios.post(
        "/order/invoice",
        {
          orderId
        },
        {
          responseType: "blob"
        }
      );

      //CREATE BLOB FROM PDF STREAM ...

      const file = new Blob([response.data], { type: "application/pdf" });

      //BUILD URL FROM FILE ...
      const fileURL = URL.createObjectURL(file);

      //Open the URL on new Window
      window.open(fileURL);
    } catch (error) {
      if (error.response !== undefined) {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};

// DELETE ORDER ...
const deleteOrderStart = () => {
  return {
    type: actionTypes.DELETE_ORDER_START
  };
};
const deleteOrderSuccess = response => {
  return {
    type: actionTypes.DELETE_ORDER_SUCCESS,
    response
  };
};
const deleteOrderFail = errors => {
  return {
    type: actionTypes.DELETE_ORDER_FAIL,
    errors: errors
  };
};

export const deleteOrder = orderId => {
  return async dispatch => {
    try {
      dispatch(deleteOrderStart());
      const response = await axios.post("/graphql", {
        query: GraphQLQueries.deleteOrderQuery,
        orderId
      });
      dispatch(deleteOrderSuccess(response.data.data.deleteOrder));
      dispatch(getOrders());
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.data.errors[0].errors) {
          dispatch(deleteOrderFail(error.response.data.errors[0].errors));
        } else {
          dispatch(shopSomethingWentWrong(error.message));
        }
      } else {
        dispatch(shopSomethingWentWrong(error.message));
      }
    }
  };
};
