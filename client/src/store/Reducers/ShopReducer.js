import { updateObject } from "../../Util/Util";
import { actionTypes } from "../Actions/ActionTypes";

const initialState = {
  products: [],
  cartProducts: [],
  orders: [],
  product: null,
  isEditing: false,
  itemsCountPerPage: null,
  totalPrice: null,
  totalItemsCount: null,
  shopLoading: false,
  shopErrors: null,
  somethingWentWrong: null
};
const changeEditingStatus = (state, action) => {
  return updateObject(state, {
    isEditing: action.isEditing
  });
};

const shopSomethingWentWrong = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    somethingWentWrong: action.somethingWentWrong
  });
};
const shopSomethingWentWrongCloseHandler = state => {
  return updateObject(state, {
    somethingWentWrong: null
  });
};

// GET PRODUCTS ...
const getProductsStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const getProductsSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null,
    products: action.response.products,
    itemsCountPerPage: action.response.itemsCountPerPage,
    totalItemsCount: action.response.totalItemsCount
  });
};
const getProductsFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};
// GET ADMIN PRODUCTS ...
const getAdminProductsStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const getAdminProductsSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null,
    products: action.response.products,
    itemsCountPerPage: action.response.itemsCountPerPage,
    totalItemsCount: action.response.totalItemsCount
  });
};
const getAdminProductsFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// GET CART PRODUCT START PRODUCTS ...
const getCartProductsStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const getCartProductsSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null,
    cartProducts: action.response.products
  });
};
const getCartProductsFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// GET PRODUCT BY ID ...
const getProductStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};

const getProductSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null,
    product: action.response
  });
};

const getProductFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// ADD PRODUCT ...
const addProductStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const addProductSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null
  });
};
const addProductFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// ADD PRODUCT TO CART...1
const addProductToCartStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const addProductToCartSuccess = state => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null
  });
};
const addProductToCartFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// EDIT PRODUCT ...
const editProductStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const editProductSuccess = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: null
  });
};
const editProductFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// GET CHECKOUT DETAILS...
const getCheckoutDetailsStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const getCheckoutDetailsSuccess = (state, action) => {
  return updateObject(state, {
    shopErrors: null,
    cartProducts: action.response.products,
    totalPrice: action.response.totalPrice,
    shopLoading: false
  });
};
const getCheckoutDetailsFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// GET ORDERS...
const getOrdersStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const getOrdersSuccess = (state, action) => {
  return updateObject(state, {
    shopErrors: null,
    orders: action.response.orders,
    shopLoading: false
  });
};
const getOrdersFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// CREATE ORDER...
const createOrderStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const createOrderSuccess = (state, action) => {
  return updateObject(state, {
    shopErrors: null,
    shopLoading: false
  });
};
const createOrderFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

// DELETE ORDER...
const deleteOrderStart = state => {
  return updateObject(state, {
    shopLoading: true
  });
};
const deleteOrderSuccess = (state, action) => {
  return updateObject(state, {
    shopErrors: null,
    shopLoading: false
  });
};
const deleteOrderFail = (state, action) => {
  return updateObject(state, {
    shopLoading: false,
    shopErrors: action.errors
  });
};

const ShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_EDITING_STATUS:
      return changeEditingStatus(state, action);

    case actionTypes.SHOP_SOMETHING_WENT_WRONG:
      return shopSomethingWentWrong(state, action);
    case actionTypes.SHOP_SOMETHING_WENT_WRONG_CLOSE:
      return shopSomethingWentWrongCloseHandler(state);

    case actionTypes.GET_PRODUCTS_START:
      return getProductsStart(state);
    case actionTypes.GET_PRODUCTS_SUCCESS:
      return getProductsSuccess(state, action);
    case actionTypes.GET_PRODUCTS_FAIL:
      return getProductsFail(state, action);

    case actionTypes.GET_ADMIN_PRODUCTS_START:
      return getAdminProductsStart(state);
    case actionTypes.GET_ADMIN_PRODUCTS_SUCCESS:
      return getAdminProductsSuccess(state, action);
    case actionTypes.GET_ADMIN_PRODUCTS_FAIL:
      return getAdminProductsFail(state, action);

    case actionTypes.GET_CART_PRODUCTS_START:
      return getCartProductsStart(state);
    case actionTypes.GET_CART_PRODUCTS_SUCCESS:
      return getCartProductsSuccess(state, action);
    case actionTypes.GET_CART_PRODUCTS_FAIL:
      return getCartProductsFail(state, action);

    case actionTypes.GET_PRODUCT_START:
      return getProductStart(state);
    case actionTypes.GET_PRODUCT_SUCCESS:
      return getProductSuccess(state, action);
    case actionTypes.GET_PRODUCT_FAIL:
      return getProductFail(state, action);

    case actionTypes.ADD_PRODUCT_START:
      return addProductStart(state);
    case actionTypes.ADD_PRODUCT_SUCCESS:
      return addProductSuccess(state, action);
    case actionTypes.ADD_PRODUCT_FAIL:
      return addProductFail(state, action);

    case actionTypes.ADD_PRODUCT_TO_CART_START:
      return addProductToCartStart(state);
    case actionTypes.ADD_PRODUCT_TO_CART_SUCCESS:
      return addProductToCartSuccess(state);
    case actionTypes.ADD_PRODUCT_TO_CART_FAIL:
      return addProductToCartFail(state, action);

    case actionTypes.EDIT_PRODUCT_START:
      return editProductStart(state);
    case actionTypes.EDIT_PRODUCT_SUCCESS:
      return editProductSuccess(state, action);
    case actionTypes.EDIT_PRODUCT_FAIL:
      return editProductFail(state, action);

    case actionTypes.GET_CHECKOUT_DETAILS_START:
      return getCheckoutDetailsStart(state);
    case actionTypes.GET_CHECKOUT_DETAILS_SUCCESS:
      return getCheckoutDetailsSuccess(state, action);
    case actionTypes.GET_CHECKOUT_DETAILS_FAIL:
      return getCheckoutDetailsFail(state, action);

    case actionTypes.GET_ORDERS_START:
      return getOrdersStart(state);
    case actionTypes.GET_ORDERS_SUCCESS:
      return getOrdersSuccess(state, action);
    case actionTypes.GET_ORDERS_FAIL:
      return getOrdersFail(state, action);

    case actionTypes.CREATE_ORDER_START:
      return createOrderStart(state);
    case actionTypes.CREATE_ORDER_SUCCESS:
      return createOrderSuccess(state, action);
    case actionTypes.CREATE_ORDER_FAIL:
      return createOrderFail(state, action);

    case actionTypes.DELETE_ORDER_START:
      return deleteOrderStart(state);
    case actionTypes.DELETE_ORDER_SUCCESS:
      return deleteOrderSuccess(state, action);
    case actionTypes.DELETE_ORDER_FAIL:
      return deleteOrderFail(state, action);

    default:
      return state;
  }
};

export default ShopReducer;
