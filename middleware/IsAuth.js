const jwt = require("jsonwebtoken");

module.exports = (request, response, next) => {
  const authHeader = request.get("Authorization");

  if (!authHeader) {
    request.isAuth = false;
    return next();
  }
  const token = authHeader.split(" ")[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, process.env.SECRET_OR_KEY);
  } catch (error) {
    request.isAuth = false;
    return next();
  }
  if (!decodedToken) {
    request.isAuth = false;
    return next();
  }
  request.isAuth = true;
  request.user = decodedToken;
  next();
};
