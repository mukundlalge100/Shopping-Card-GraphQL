const validator = require("validator");

const isEmpty = require("../util/isEmpty");

module.exports = validateNewPasswordInput = data => {
  let errors = {};
  const password = data.password;
  const confirmPassword = data.confirmPassword;

  if (isEmpty(password)) {
    errors.passwordIsNotValid = "Password field is required!";
  } else if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.passwordIsNotValid =
      "Password must be in between 6 and 30 characters!";
  }
  if (isEmpty(password)) {
    errors.confirmPasswordIsNotValid = "Confirm Password field is required!";
  }

  if (!validator.equals(password, confirmPassword)) {
    errors.passwordsAreNotMatch = "Passwords must match!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
