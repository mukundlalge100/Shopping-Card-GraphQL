const validator = require("validator");

const isEmpty = require("../util/isEmpty");

module.exports = validateUserLogInInput = data => {
  let errors = {};
  const email = data.email;
  const password = data.password;
  if (isEmpty(email)) {
    errors.emailIsNotValid = "Email field is required!";
  } else if (!validator.isEmail(email)) {
    errors.emailIsNotValid = "Email is not valid,please enter valid email!";
  }
  if (isEmpty(password)) {
    errors.passwordIsNotValid = "Password field is required!";
  } else if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.passwordIsNotValid =
      "Password must be in between 6 and 30 characters!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
