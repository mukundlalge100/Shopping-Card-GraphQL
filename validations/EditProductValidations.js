const validator = require("validator");

const isEmpty = require("../util/isEmpty");

// VALIDATE USER INPUT FOR EDIT PRODUCT PAGE...
module.exports = validateEditProductInput = data => {
  let errors = {};
  const title = data.title;
  const imageUrl = data.imageUrl.trim();
  const price = data.price;
  const description = data.description;

  if (isEmpty(title)) {
    errors.titleIsNotValid = "title field is required!";
  } else if (!validator.isLength(title, { min: 4, max: 30 })) {
    errors.titleIsNotValid = "Title must be in between 4 and 30 characters!";
  }

  if (isEmpty(imageUrl)) {
    errors.imageUrlIsNotValid = "imageUrl is not valid !";
  }

  if (isEmpty(price)) {
    errors.priceIsNotValid = "price field is required!";
  } else if (!validator.isNumeric(price)) {
    errors.priceIsNotValid = "price is not valid,please enter valid email!";
  }

  if (isEmpty(description)) {
    errors.descriptionIsNotValid = "description field is required!";
  } else if (!validator.isLength(description, { min: 10, max: 400 })) {
    errors.descriptionIsNotValid =
      "Description must be in between 6 and 400 characters!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
