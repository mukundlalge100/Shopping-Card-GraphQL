const validator = require("validator");

const isEmpty = require("../util/isEmpty");

module.exports = validateUserSignUpInput = data => {
  let errors = {};
  const email = validator.normalizeEmail(data.email);
  const password = data.password;
  const mobileNumber = data.mobileNumber;
  const confirmPassword = data.confirmPassword;
  const userName = data.userName;

  if (isEmpty(userName)) {
    errors.userNameIsNotValid = "User Name field is required!";
  } else if (!validator.isLength(userName, { min: 2, max: 30 })) {
    errors.userNameIsNotValid =
      "User Name must be in between 2 and 30 characters!";
  }
  if (isEmpty(mobileNumber)) {
    errors.mobileNumberIsNotValid = "Mobile number field is required!";
  } else if (!validator.isMobilePhone(mobileNumber, "en-IN")) {
    errors.mobileNumberIsNotValid = "Mobile Number is not valid";
  }
  if (isEmpty(email)) {
    errors.emailIsNotValid = "Email field is required!";
  } else if (!validator.isEmail(email)) {
    errors.emailIsNotValid = "Email is not valid,please enter valid email!";
  }
  if (isEmpty(password)) {
    errors.passwordIsNotValid = "Password field is required!";
  } else if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.passwordIsNotValid =
      "Password must be in between 6 and 30 characters!";
  }
  if (isEmpty(confirmPassword)) {
    errors.confirmPasswordIsNotValid = "Confirm Password field is required!";
  }

  if (!validator.equals(password, confirmPassword)) {
    errors.passwordsAreNotMatch = "Passwords must match!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
