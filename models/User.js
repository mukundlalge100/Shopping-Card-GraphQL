const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserScema = new Schema({
  userName: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  cart: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: "Product",
          required: true
        },
        quantity: { type: Number, required: true }
      }
    ]
  },
  resetToken: {
    type: String
  },
  resetTokenExpiration: {
    type: Date
  }
});
module.exports = mongoose.model("User", UserScema);
