const authController = require("../../controllers/AuthControllers/AuthController");
const shopController = require("../../controllers/ShopControllers/ShopController");
const adminController = require("../../controllers/AdminControllers/AdminController");
const OrderController = require("../../controllers/OrderControllers/OrderController");
module.exports = {
  // MUTATIONS RESOLVERS ...

  // CREATE USER ...
  createUser: ({ data }, request) => authController.postSignUp(data, request),

  // PASSWORD RESET ...
  postResetPassword: ({ data }, request) =>
    authController.postPasswordReset(data, request),

  // NEW PASSWORD SET...
  postNewPassword: ({ data }, request) =>
    authController.postNewPassword(data, request),

  // ADD PRODUCT TO CART ...
  addProductToCart: ({ data }, request) =>
    shopController.postAddProductToCart(data, request),

  //DELETE PRODUCT ...
  deleteProduct: ({ data }, request) =>
    adminController.deleteProduct(data, request),

  // DELETE PRODUCT FROM CART ...
  deleteProductFromCart: ({ data }, request) =>
    shopController.postCartDeleteProduct(data, request),

  //INCREMENT OR DECREMENT OF PRODUCT FROM CART ...
  decrementQuantity: ({ data }, request) =>
    shopController.decrementQuantityOfProduct(data, request),
  incrementQuantity: ({ data }, request) =>
    shopController.incrementQuantityOfProduct(data, request),

  // CREATE ORDER ...
  postOrder: ({ data }, request) => OrderController.postOrder(data, request),

  // DELETE ORDER ...
  deleteOrder: ({ data }, request) =>
    OrderController.deleteOrder(data, request),

  // QUERY RESOLVERS ...

  logIn: ({ data }, request) => authController.postLogIn(data, request),

  validateEmail: ({ data }, request) =>
    authController.validateEmail(data, request),

  getProducts: ({ data }, request) => shopController.getProducts(data, request),

  getAdminProducts: ({ data }, request) =>
    adminController.getProductList(data, request),

  getCartProducts: ({ data }, request) =>
    shopController.getCartProducts(data, request),

  getProduct: ({ data }, request) => shopController.getProduct(data, request),

  getCheckoutDetails: ({ data }, request) =>
    shopController.getCheckoutDetails(data, request),

  logOut: ({ data }, request) => authController.postLogOut(data, request),

  getOrders: ({ data }, request) => OrderController.getOrders(data, request)

  // SUBSCRIPTION RESOLVERS ...
};
