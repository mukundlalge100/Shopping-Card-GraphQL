const { buildSchema } = require("graphql");

module.exports = buildSchema(
  `
  type User {
    _id: ID!
    userName: String
    email: String
    mobileNumber:String
    password: String
    cart:[Items!]!
    date: String 
  }

  type Order {
    _id:ID!
    user:User! 
    products:[ProductInfo!]!
    totalPrice:Int!
  }

  type ProductInfo {
    product: Product! 
    quantity:Int! 
  }

  type Product {
    _id:ID!
    title:String! 
    imageUrl:String!
    price:Int!
    description:String! 
    userId :User! 
  }
  
  type Items {
    productId:Product!
    quantity:Int!
  }


  

  type Query {
    logIn(data:LogInInput!):AuthData!
    validateEmail(data:ValidateEmailInput!) : ValidateEmailResponse!
    getProducts:GetProductsResponse!
    getProduct:Product!
    logOut : LogOutResponse!  
    getAdminProducts:GetProductsResponse! 
    getCartProducts:GetCartProductsResponse! 
    getCheckoutDetails:GetCheckoutDetailsResponse!
    getOrders:GetOrdersResponse! 
  }
  type GetOrdersResponse {
    orders:[Order!]!
  }
  type GetCheckoutDetailsResponse {
    products :[CartProduct!]!
    totalPrice:Int!
  }
  type GetCartProductsResponse {
    products : [CartProduct!]
  }
  type CartProduct {
    _id :ID!
    productId:Product! 
    quantity:Int!
  }

  type ValidateEmailResponse {
    success : Boolean!
    message : String!
  }
  
  type LogOutResponse {
    success:Boolean! 
    message:String!
  }
  type GetProductsResponse {
    products:[Product!]!
    itemsCountPerPage:Int!
    totalItemsCount:Int!
  }

  type AuthData {
    token:String!
  }
  input ValidateEmailInput {
    email:String!
  }
  input LogInInput {
    email:String!
    password:String!
  }


  

  
  type Mutation {
    createUser(data: CreateUserInput!): NormalResponse!
    postResetPassword(data:ResetPasswordInput!) :NormalResponse!
    postNewPassword(data:NewPasswordInput!) :NormalResponse!

    deleteProduct:NormalResponse!
    addProduct(data:AddProductInput!) : Product
    addProductToCart:NormalResponse! 
    deleteProductFromCart:NormalResponse!

    incrementQuantity:NormalResponse!
    decrementQuantity:NormalResponse!

    postOrder:NormalResponse!
    deleteOrder:NormalResponse! 
  }
  type NormalResponse {
    success:Boolean! 
    message:String!
  }
  input DeleteProductInput {
    productId:Int! 
  }
  input AddProductInput {
    title:String!
    description:String! 
    price:String! 
  }

  input NewPasswordInput {
    password:String!
    confirmPassword:String! 
    token:String! 
  }

  input ResetPasswordInput {
    email:String!
  }
  input CreateUserInput {
    userName: String!
    email: String!
    confirmPassword:String!
    password: String!
    mobileNumber:String!
  }



  schema {
    mutation:Mutation
    query:Query
  }
`
);
