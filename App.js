// GLOBAL IMPORTS FOR MODULES ...
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
//ENVIRONMENT FILE CONFIG ...
const dotenv = require("dotenv");
dotenv.config({ path: "./config/config.env" });

const passport = require("passport");
const isAuth = require("./middleware/IsAuth");
const fileUpload = require("./services/FileUpload");
const mongoose = require("mongoose");

// GRAPHQL IMPORTS ...
const expressGraphQL = require("express-graphql");
const Resolvers = require("./GraphQL/Resolvers/Resolvers");
const GraphQLSchema = require("./GraphQL/Schema/Schema");

// CONFIG FOR SESSIONS SO IT WILL AVAILABLE IN REQUEST ...
const session = require("express-session");
const MongoDbStore = require("connect-mongodb-session")(session);

const adminRoutes = require("./routes/Admin");
const shopRoutes = require("./routes/Shop");

// CONFIG FOR BODY PARSER SO BODY OF REQUEST WILL BE AVAILABLE...
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS SETTING ...
app.use((request, response, next) => {
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS,PUT,POST,GET,PATCH,DELETE"
  );
  response.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type,Authorization,X-JSON,x-requested-with, X-Auth-Token"
  );
  if (request.method === "OPTIONS") {
    return response.sendStatus(200);
  }
  next();
});

// CONNECTION TO DB...
const port = process.env.PORT || 5000;

mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then((result) => {
    console.log("Server is running on port 5000!\nMongoDB Connected!");
    app.listen(port);
  })
  .catch((error) => {
    console.log(error);
  });

const store = new MongoDbStore({
  uri: process.env.MONGODB_URI,
  collection: "sessions",
  expires: 86400000,
});

// ISAUTH CONFIG ...
require("./config/passport")(passport);
app.use(isAuth);

// SESSION MIDDLEWARE...
app.use(
  session({
    secret: "12345678",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);

// MULTER FILE UPLOADER CONFIG, MULTIPART FORM CONFIG ...
app.use(fileUpload.single("imageUrl"));
app.use("/admin", adminRoutes);
app.use("/order", shopRoutes);

// GRAPHQL CONFIGURATION ...
app.use(
  "/graphql",
  expressGraphQL({
    graphiql: true,
    schema: GraphQLSchema,
    rootValue: Resolvers,
    customFormatErrorFn(error) {
      if (!error.originalError) {
        return error;
      }
      const errors = error.originalError.errors;
      const message = error.message;
      const status = error.originalError.status;
      console.log(error);
      return { errors, message, status };
    },
    pretty: true,
  })
);

// SERVER STATIC ASSETS IF IN PRODUCTION ...
if (process.env.NODE_ENV === "production") {
  // SET STATIC FOLDER ...
  app.use(express.static(path.join(__dirname, "client", "build")));
  app.get("*", (request, response) => {
    response.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}
